#![feature(duration_as_u128)]

extern crate rand;
extern crate sorts;

use rand::random;
use std::time::Instant;

fn help() {
    eprintln!("Please provide sort algorithm: quick radix default intro insert heap");
    std::process::exit(1);
}

enum SortType {
    Quick,
    Radix,
    Def,
    Merge,
    Heap,
    Insert,
    Intro,
}

fn do_sort(mut arr: &mut [i64], typ: SortType) -> u128 {
    let len = arr.len();
    let start = Instant::now();
    match typ {
        SortType::Quick => {
            sorts::quick_sort(&mut arr, 0, len - 1);
        }
        SortType::Radix => {
            sorts::radix_sort(&mut arr);
        }
        SortType::Def => {
            arr.sort();
        }
        SortType::Merge => {
            sorts::merge_sort(&mut arr, 0, len - 1);
        }
        SortType::Heap => {
            sorts::heapster(&mut arr);
        }
        SortType::Insert => {
            sorts::insert_sort(&mut arr);
        }
        SortType::Intro => {
            sorts::intro_sort(&mut arr);
        }
    }
    let stop = start.elapsed();
    stop.as_millis()
}

fn main() {
    let mut which = SortType::Def;
    let opt: Vec<String> = std::env::args().collect();
    if opt.len() < 2 {
        help();
    } else {
        match opt[1].to_lowercase().as_ref() {
            "radix" => which = SortType::Radix,
            "quick" => which = SortType::Quick,
            "default" => which = SortType::Def,
            "intro" => which = SortType::Intro,
            "insert" => which = SortType::Insert,
            "heap" => which = SortType::Heap,
            "merge" => which = SortType::Merge,
            _ => help(),
        }
    }
    let mut no: usize = 1000000;
    let mut vals: Vec<i64> = vec![];
    if opt.len() == 3 {
        if let Ok(number) = opt[2].parse::<usize>() {
            no = number;
        }

    }

    for _ in 0..no {
        let val: i64 = random();
        vals.push(val);
    }

    let mut vclone = vals.clone();
    vals.sort();

    let val = do_sort(&mut vclone, which);
    assert_eq!(vals, vclone);
    println!("{} Sort took {} milliseconds", opt[1], val);
}
