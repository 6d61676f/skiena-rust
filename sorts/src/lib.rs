use std::collections::VecDeque;

fn swap<T: Clone + PartialEq>(arr: &mut [T], x: usize, y: usize) {
    if x == y || arr[x] == arr[y] {
        return;
    }

    let temp = arr[x].clone();
    arr[x] = arr[y].clone();
    arr[y] = temp;
}

pub fn heapster<T: Clone + PartialOrd>(arr: &mut [T]) {
    let len = arr.len();
    for i in (1..(len / 2) + 1).rev() {
        _bubble_down(arr, i, len);
    }
    _minimize(arr);
}

fn _minimize<T: Clone + PartialOrd>(arr: &mut [T]) {
    let mut len = arr.len();
    while _get_min(arr, len) == true {
        len -= 1;
    }
}

fn _get_min<T: Clone + PartialOrd>(arr: &mut [T], len: usize) -> bool {
    if len < 1 {
        return false;
    }
    swap(arr, 0, len - 1);
    _bubble_down(arr, 1, len - 1);
    true
}

fn _bubble_down<T: Clone + PartialOrd>(arr: &mut [T], node: usize, len: usize) {
    let lchild_node = 2 * node;
    let rchild_node = 2 * node + 1;
    let mut min_node = node;

    for i in &[lchild_node, rchild_node] {
        if *i <= len {
            if arr[min_node - 1] < arr[*i - 1] {
                min_node = *i;
            }
        }
    }

    if min_node != node {
        swap(arr, min_node - 1, node - 1);
        _bubble_down(arr, min_node, len);
    }
}

pub fn insert_sort<T: Clone + PartialOrd>(arr: &mut [T]) {
    let len = arr.len();
    for i in 1..len {
        let mut j = i;
        while j > 0 && arr[j] < arr[j - 1] {
            swap(arr, j, j - 1);
            j -= 1;
        }
    }
}

fn merge<T: PartialOrd + Clone>(arr: &mut [T], low: usize, mid: usize, high: usize) {
    let mut q1: VecDeque<T> = VecDeque::default();
    let mut q2: VecDeque<T> = VecDeque::default();
    let mut index = low;

    for i in low..mid + 1 {
        q1.push_back(arr[i].clone());
    }

    for i in mid + 1..high + 1 {
        q2.push_back(arr[i].clone());
    }

    while q1.len() > 0 && q2.len() > 0 {
        if q1.front().unwrap() <= q2.front().unwrap() {
            arr[index] = q1.pop_front().unwrap();
        } else {
            arr[index] = q2.pop_front().unwrap();
        }
        index += 1;
    }

    while let Some(val) = q1.pop_front() {
        arr[index] = val;
        index += 1;
    }
    while let Some(val) = q2.pop_front() {
        arr[index] = val;
        index += 1;
    }
}

pub fn merge_sort<T: PartialOrd + Clone>(arr: &mut [T], low: usize, high: usize) {
    let middle: usize;
    if low < high {
        middle = (low + high) / 2;
        merge_sort(arr, low, middle);
        merge_sort(arr, middle + 1, high);
        merge(arr, low, middle, high);
    }
}

fn q_partition<T: PartialOrd + Clone>(arr: &mut [T], low: usize, high: usize) -> usize {
    //The median method is not actually necesarry
    {
        let mid = (low + high) / 2;
        if arr[mid] < arr[low] {
            swap(arr, mid, low);
        }
        if arr[high] < arr[low] {
            swap(arr, high, low);
        }
        if arr[high] < arr[mid] {
            swap(arr, mid, high);
        }
    }

    let p = high;
    let mut firsthigh = low;

    for i in low..high {
        if arr[i] < arr[p] {
            swap(arr, i, firsthigh);
            firsthigh += 1;
        }
    }

    swap(arr, p, firsthigh);
    firsthigh
}

pub fn quick_sort<T: PartialOrd + Clone>(arr: &mut [T], low: usize, high: usize) {
    if high > low {
        let p = q_partition(arr, low, high);
        quick_sort(arr, low, p - 1);
        quick_sort(arr, p + 1, high);
    }
}

pub fn radix_sort(arr: &mut [i64]) {
    let mut buckets: Vec<VecDeque<i64>> = Vec::new();
    for _ in 0..19 {
        buckets.push(VecDeque::new());
    }
    let mut digit_index = 1;
    loop {
        for i in 0..arr.len() {
            let mut digit = arr[i];
            for _ in 1..digit_index {
                digit = digit / 10;
            }
            digit = digit % 10;
            if digit < 0 {
                digit -= 9;
            }
            buckets[digit.abs() as usize].push_back(arr[i]);
        }
        if buckets[0].len() == arr.len() {
            break;
        }
        let mut arr_index = 0;
        while arr_index < arr.len() {
            for j in (10..19).rev() {
                while buckets[j].len() != 0 {
                    arr[arr_index] = buckets[j].pop_front().unwrap();
                    arr_index += 1;
                }
            }
            for j in 0..10 {
                while buckets[j].len() != 0 {
                    arr[arr_index] = buckets[j].pop_front().unwrap();
                    arr_index += 1;
                }
            }
        }
        digit_index += 1;
    }
}

pub fn intro_sort<T: Clone + PartialOrd>(arr: &mut [T]) {
    let maxdepth = ((arr.len() as f64).log2() as usize) * 2;
    _intro_sort(arr, maxdepth);
}

fn _intro_sort<T: Clone + PartialOrd>(arr: &mut [T], maxdepth: usize) {
    let len = arr.len();
    if len <= 1 {
        return;
    }
    let p = q_partition(arr, 0, len - 1);
    if maxdepth == 0 {
        heapster(arr);
    } else {
        _intro_sort(&mut arr[0..p], maxdepth - 1);
        _intro_sort(&mut arr[p + 1..len], maxdepth - 1);
    }
}
