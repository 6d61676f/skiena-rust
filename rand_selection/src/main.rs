fn swap<T>(arr: &mut [T], i: usize, j: usize)
where
    T: PartialOrd + Clone,
{
    if i == j || arr[i] == arr[j] {
        return;
    }
    let t = arr[i].clone();
    arr[i] = arr[j].clone();
    arr[j] = t;
}

fn liner_selection<T>(arr: &mut [T], i_order: usize) -> &T
where
    T: PartialOrd + Clone + std::fmt::Debug,
{
    let p = __q_partition(arr);
    if p > i_order {
        liner_selection(&mut arr[..p], i_order)
    } else if p < i_order {
        liner_selection(&mut arr[(p + 1)..], i_order - p)
    } else {
        &arr[p]
    }
}

fn __q_partition<T>(arr: &mut [T]) -> usize
where
    T: PartialOrd + Clone + std::fmt::Debug,
{
    //medians of medians
    {
        let low = 0;
        let high = arr.len() - 1;
        let med = arr.len() / 2;

        if arr[low] > arr[high] {
            swap(arr, low, high);
        }
        if arr[low] > arr[med] {
            swap(arr, low, med);
        }
        if arr[med] > arr[high] {
            swap(arr, med, high);
        }
    }

    let mut firsthigh = 0;
    let p = arr.len() - 1;

    for i in 0..arr.len() {
        if arr[i] < arr[p] {
            swap(arr, i, firsthigh);
            firsthigh += 1;
        }
    }
    swap(arr, p, firsthigh);
    firsthigh
}

fn main() {
    let mut arr = [5, 2, 10, 23, 4, 90].to_vec();
    let val = liner_selection(arr.as_mut(), 1);
    println!("{:?}", val);
}
