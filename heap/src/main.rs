use std::collections::VecDeque;

extern crate rand;

pub enum TraversalOrder {
    InOrder,
    PreOrder,
    PostOrder,
}

pub struct Heap<T>
where
    T: Clone + PartialOrd + std::fmt::Debug,
{
    arr: VecDeque<Box<T>>,
    maxlen: usize,
    len: usize,
}


impl<'a, T> From<&'a [T]> for Heap<T>
where
    T: Clone + PartialOrd + std::fmt::Debug,
{
    fn from(vec: &'a [T]) -> Self {
        let mut h: Heap<T> = Heap::new(vec.len());
        for val in vec {
            h.insert(val.clone());
        }
        h
    }
}

impl<T> Heap<T>
where
    T: Clone + PartialOrd + std::fmt::Debug,
{
    pub fn new(max_len: usize) -> Heap<T> {
        Heap {
            arr: VecDeque::new(),
            maxlen: (max_len + 1),
            len: 0,
        }
    }

    fn lchild(index: usize) -> usize {
        2 * index
    }

    fn rchild(index: usize) -> usize {
        2 * index + 1
    }

    fn parent(index: usize) -> usize {
        index / 2
    }

    fn bubble_up(&mut self, node: usize) {
        if node <= 1 {
            return;
        }

        let parent = <Heap<T>>::parent(node);

        if self.arr[node] < self.arr[parent] {
            self.arr.swap(node, parent);
            self.bubble_up(parent);
        }
    }

    pub fn insert(&mut self, val: T) -> bool {
        if self.len == self.maxlen {
            return false;
        }
        if self.len == 0 {
            self.arr.push_back(Box::new(val.clone()));
        }
        self.len += 1;
        self.arr.push_back(Box::new(val));
        let len = self.len;
        self.bubble_up(len);
        true
    }

    fn bubble_down(&mut self, node: usize) {
        let rchild_node = <Heap<T>>::rchild(node);
        let lchild_node = <Heap<T>>::lchild(node);
        let mut min_node = node;

        for i in &[lchild_node, rchild_node] {
            if *i <= self.len {
                if self.arr[min_node] > self.arr[*i] {
                    min_node = *i;
                }
            }
        }

        if min_node != node {
            self.arr.swap(min_node, node);
            self.bubble_down(min_node);
        }
    }

    pub fn get_min(&mut self) -> Option<Box<T>> {
        if self.len == 0 {
            return None;
        }
        let min = self.arr[1].clone();
        let last = self.len;

        self.arr.swap(1, last);
        self.len -= 1;
        self.bubble_down(1);
        Some(min)
    }

    pub fn print_heap_level(&self, level: usize) {
        let start = 1 << level;
        if (start) > self.len {
            return;
        } else {
            let mut i = start;
            while i < (2 * start) && i <= self.len {
                print!("{}:{:?}\t", i, self.arr[i]);
                i += 1;
            }
            println!();
            self.print_heap_level(level + 1);
        }
    }

    pub fn print_heap(&self, node: usize, order: &TraversalOrder) {
        if node == 1 {
            println!();
        }
        if node > self.len {
            println!();
            return;
        }
        let lc = <Heap<T>>::lchild(node);
        let rc = <Heap<T>>::rchild(node);
        match order {
            TraversalOrder::InOrder => {
                self.print_heap(lc, order);
                print!("{}: {:?} | ", node, self.arr[node - 1]);
                self.print_heap(rc, order);
            }
            TraversalOrder::PreOrder => {
                print!("{}: {:?} | ", node, self.arr[node - 1]);
                self.print_heap(lc, order);
                self.print_heap(rc, order);
            }
            TraversalOrder::PostOrder => {
                self.print_heap(rc, order);
                self.print_heap(lc, order);
                print!("{}: {:?} | ", node, self.arr[node - 1]);
            }
        }
    }
}

fn main() {
    let mut h = Heap::new(128);
    for _ in 0..10 {
        let val: i32 = rand::random();
        h.insert(val);
    }

    h.print_heap_level(0);
    h.print_heap(1, &TraversalOrder::InOrder);
    h.print_heap(1, &TraversalOrder::PreOrder);
    h.print_heap(1, &TraversalOrder::PostOrder);

    while let Some(val) = h.get_min() {
        println!("{}", val);
    }
}
