use std::collections::VecDeque;
use std::error::Error;

fn count_inv_sort<T: Clone + PartialOrd + std::fmt::Debug>(
    arr: &mut [T],
    inv: &mut Vec<(T, T)>,
) -> usize {
    if arr.len() <= 1 {
        0
    } else {
        let mid = arr.len() / 2;
        let mut inv_count = 0;
        inv_count += count_inv_sort(&mut arr[..mid], inv);
        inv_count += count_inv_sort(&mut arr[mid..], inv);
        inv_count += _count_split(arr, inv);
        inv_count
    }
}

fn _count_split<T: Clone + PartialOrd + std::fmt::Debug>(
    arr: &mut [T],
    inv: &mut Vec<(T, T)>,
) -> usize {
    let mut lh = VecDeque::new();
    let mut rh = VecDeque::new();
    let len = arr.len();
    let mid = len / 2;
    for i in arr[..mid].iter() {
        lh.push_back(i.clone());
    }
    for i in arr[mid..].iter() {
        rh.push_back(i.clone());
    }
    let mut k = 0;
    let mut count = 0;
    while lh.len() > 0 && rh.len() > 0 {
        if lh.front().unwrap() > rh.front().unwrap() {
            //For big inputs this wrecks the memory
            for i in lh.iter() {
                inv.push((i.clone(), rh.front().unwrap().clone()));
            }
            arr[k] = rh.pop_front().unwrap();
            count += lh.len();
        } else {
            arr[k] = lh.pop_front().unwrap();
        }
        k += 1;
    }
    for i in lh.into_iter() {
        arr[k] = i;
        k += 1;
    }
    for i in rh.into_iter() {
        arr[k] = i;
        k += 1;
    }
    count
}

fn main() {
    let mut arr = [1, 3, 5, 3, 4, 6];
    let mut inv: Vec<(i32, i32)> = Vec::new();
    let rez = count_inv_sort(&mut arr, &mut inv);
    print!("Array has {} inversions: ", rez);
    for i in inv.into_iter() {
        print!(" {:?} ", i);
    }
    //println!("\nSorted array is {:?}", arr);
}
